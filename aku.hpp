///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 3
///
/// @file aku.hpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author Geoffrey Teocson <gteocson@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   23_03_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "fish.hpp"

using namespace std;

namespace animalfarm {

class Aku : public Fish {
public:
   float fishWeight;

   Aku(float weight, enum Color newColor, enum Gender newGender );

   virtual const string speak();

   void printInfo();

};

}
