///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Geoffrey Teocson <gteocson@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   26_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {

Palila::Palila( string location, enum Color newColor, enum Gender newGender ){
   gender = newGender;
   species = "Loxioides bailleui";
   isMigratory = 0;
   featherColor = newColor;
   whereFound = location;
}

const string Palila::speak() {
   return string( "Tweet" );
}

void Palila::printInfo() {
   cout << "Palila" << endl;
   cout << "   Where Found = [" << whereFound << "]" << endl;
   Bird::printInfo();
}

}

