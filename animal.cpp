///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 3
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Geoffrey Teocson <gteocson@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   22_03_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "aku.hpp"
#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Animal::Animal(void){

   cout << ".";

}

Animal::~Animal(void){

   cout << "x";

}

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}

string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
} 
	

string Animal::colorName (enum Color color) {

   switch(color){

      case BLACK  : return string("BLACK"); break;
      case WHITE  : return string("WHITE"); break;
      case RED    : return string("RED")  ; break;
      case BLUE   : return string("BLUE") ; break;
      case GREEN  : return string("GREEN"); break;
      case PINK   : return string("PINK") ; break;
      case SILVER : return string("SILVER");break;
      case YELLOW : return string("YELLOW");break;
      case BROWN  : return string("BROWN"); break;
      }
   
      return string("Unknown");
}

const Gender Animal::getRandomGender(){

   switch(int i = rand()%2){

      case 0 : return MALE;  break;
      case 1 : return FEMALE;break;

   }
   
   return UNKNOWN;

}

const Color Animal::getRandomColor(){

   switch(int z = rand()%9){

      case 0 : return BLACK; break;
      case 1 : return WHITE; break;
      case 2 : return RED;   break;
      case 3 : return BLUE;  break;
      case 4 : return GREEN; break;
      case 5 : return PINK;  break;
      case 6 : return SILVER;break;
      case 7 : return YELLOW;break;
      case 8 : return BROWN; break;
      }

   return RAINBOW;
}

const bool Animal::getRandomBool(){

   switch(int i = rand()%2){

      case 0: return 0; break;
      case 1: return 1; break;


   }
}

const float Animal::getRandomWeight( const float from, const float to ){


   float diff = to - from;
   float randWeight = rand()%(int)diff + from;
   return randWeight;
}

const string Animal::getRandomName(){

  int length = rand()%6 + 4;

  string randomName= "";

  char lowerCase = rand()%26+97;
  char upperCase = rand()%26+65;

  randomName= randomName + upperCase;

  for (int i=1; i<length; i++)
    randomName = randomName + lowerCase;


   return randomName;
}

Animal* AnimalFactory::getRandomAnimal(){

   Animal* newAnimal = NULL;
   int g = rand() % 6;
   

   switch(g) {
      case 0: newAnimal = new Cat   (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
      case 1: newAnimal = new Dog   (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break; 
      case 2: newAnimal = new Nunu  (Animal::getRandomBool(), Animal::getRandomColor(), Animal::getRandomGender()); break;
      case 3: newAnimal = new Aku   (Animal::getRandomWeight(0,45), Animal::getRandomColor(), Animal::getRandomGender()); break;
      case 4: newAnimal = new Palila(Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
      case 5: newAnimal = new Nene  (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;

   }
   return newAnimal;

}

} // namespace animalfarm
